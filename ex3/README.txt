Advanced Methods in Natural Language Processing – Spring 2018 - Assignment 3

################### BY ################################

Dana Cohen - cohedana@gmail.com - 314865973
Tomer Ronen - tomer.ronen34@gmail.com - 308492909
Or Latovitz - orlatovitz@gmail.com - 206261331

################### Results Summary ###################

Most frequent tag baseline - the accuracy on the development set is 0.93
HMM tagger - 
Our pruning policy is to check if the emission probability of a tag and a word is zero and if it is 
we do not iterate through the all the possible options of previous tags to check for path, because all of this solutions
are not relevant.
Our optimal lambda are lambda1 = 0.68, lambda2 = 0.25
the accuracy on the development set is 0.96

MEMM tagger - the accuracy on the development set is 0.612
False Analysis:
We create logs to documents out mistakes and analyze them.
We observed an that 0.23% of the mistakes are unknown words although they are only 0.03% of the data,
which mean that if we could decrease the number of the unknown words then the number of the mistakes will decrease
significantly.
We alsp created a confusion matrix between the predicted tag and the correct tag to examine some of our common mistakes:
1. Replacement between NN and JJ - nouns and adjectives, because there words who function as both - 
for example, the word "video" might be adjective in "video games" but noun by itself. In addition,
both of the tags are very common in the dataset(('NN', 132935) ('JJ', 61217))
 {
    "correct_tag": "JJ", 
    "predicted_tag": "NN", 
    "context": {
      "prevprev_word": "maker", 
      "prev_tag": "IN", 
      "curr_word": "video", 
      "next_word": "games", 
      "prevprev_tag": "NN", 
      "prev_word": "of"
  }
  
2. Replacment between NNPS and NNP - we saw cases where there is a
proper noun which consists  from  several nouns and the tagger tags each inner nouns separately,
or where the proper noun has "s" at the end of the word but it is still refer to a singular proper noun.
for example:
  {
    "correct_tag": "NNP", 
    "predicted_tag": "NNPS", 
    "context": {
      "prevprev_word": "for", 
      "prev_tag": "DT", 
      "curr_word": "Giants", 
      "next_word": "today", 
      "prevprev_tag": "IN", 
      "prev_word": "the"
    }

3. Replacment between adverb and preposition, this is because some word that are preposition
can act as adverb depends on the tag and the meaning of the following word.
for example:
 {
    "correct_tag": "RB", 
    "predicted_tag": "IN", 
    "context": {
      "prevprev_word": "loss", 
      "prev_tag": "IN", 
      "curr_word": "around", 
      "next_word": "10", 
      "prevprev_tag": "NN", 
      "prev_word": "of"
  }
	
4. Replacment between between NN and NNP, especially in unknown words, although some of them are replaced in
"CAPITAL_LETTER" instead of "UNK". This might be because nouns at the beginning of the sentence also appear with 
capital letter and therefore this feature is not indicative enough. In addition,
both of the tags are very common in the dataset(('NN', 132935),('NNP', 91466))

5. Replacment between VBN and VBD in verbs which their v2 and v3 forms are the same, and in order to understand 
the tense you have to look at the whole sentence and not only at the last two previous words, for example:
   {
    "correct_tag": "VBD", 
    "predicted_tag": "VBN", 
    "context": {
      "prevprev_word": "committee", 
      "prev_tag": "NNS", 
      "curr_word": "disapproved", 
      "next_word": ",", 
      "prevprev_tag": "NN", 
      "prev_word": "members"
    }
we added the confusion matrix and the logs to the solution fo the assignment.

