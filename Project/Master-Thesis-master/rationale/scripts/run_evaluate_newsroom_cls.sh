#!/usr/bin/env bash

EXP_NAME=newsroom_cls1
GPU=1

EXP_DIR=exp_${EXP_NAME}
TEE_LOG=${EXP_DIR}/tee_log.txt

mkdir -p ${EXP_DIR}
exec > >(tee -a ${TEE_LOG}) 2>&1

PYTHONUNBUFFERED=1 \
CUDA_VISIBLE_DEVICES=${GPU} \
python rationale_dependent.py \
--newsroom_data True \
--train "" \
--dev "/home/dana/newsroom_exp_data/train.jsonl.gzip" \
--embedding "/home/dana/nlp/shared/embeddings/glove.6B/glove.6B.300d.txt.gz" \
--classification     --nclasses=5    \
--dump "exp_${EXP_NAME}/rationales.json"  \
--load_rationale=""     \
--save_debug_tensors \
--exp_num="_${EXP_NAME}" \
--hidden_dimension=300 \
--evaluate_cls_model \
--load_model="/home/dana/nlp/dana/Master-Thesis-master/rationale/exp_newsroom_cls1_copy/saves/best_model_0.9850.ckpt"
