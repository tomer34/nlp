id_1 = "308492909"
id_2 = "314865973"
id_3 = "206261331"
email = "tomer.ronen34@gmail.com"

def get_details():
    if (not id_1) or (not id_2) or not (email):
        raise Exception("Missing submitters info")

    info = str.format("{}_{}_{}      email: {}", id_1, id_2, id_3, email)

    return info