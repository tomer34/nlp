from data import *
from sklearn.feature_extraction import DictVectorizer
from sklearn import linear_model
import time
from submitters_details import get_details
import datetime
import os.path as osp
import cPickle as pkl
import numpy as np
import json
from confusion_matrix import plot_confusion_matrix


def extract_features_base(curr_word, next_word, prev_word, prevprev_word, prev_tag, prevprev_tag):
    """
        Receives: a word's local information
        Returns: The word's features.
    """
    features = {}
    features['word'] = curr_word

    ### YOUR CODE HERE

    if uni_features:
        # words
        features['next_word'] = next_word
        features['prev_word'] = prev_word
        features['prevprev_word'] = prevprev_word

        # tags
        features['prev_tag'] = prev_tag
        features['prevprev_tag'] = prevprev_tag

    if bi_features:
        # words
        # features['prevprev_word+prev_word'] = prevprev_word + '+' + prev_word
        # features['prev_word+next_word'] = prev_word + '+' + next_word

        # tags
        features['prevprev_tag+prev_tag'] = prevprev_tag + '+' + prev_tag

        # word + tag
        features['prev_word+prev_tag'] = prev_word + '+' + prev_tag
        features['prevprev_word+prevprev_tag'] = prevprev_word + '+' + prevprev_tag

    if prefix_suffix_features:
        # prefix and suffix params
        min_prefix = 2
        max_prefix = 4
        min_suffix = 1
        max_suffix = 4

        # prefix
        for prefix_len in range(min_prefix, max_prefix + 1):
            features['prefix_%d' % prefix_len] = curr_word[:prefix_len]

        # suffix
        for suffix_len in range(min_suffix, max_suffix + 1):
            features['suffix_%d' % suffix_len] = curr_word[-suffix_len:]

        # prev_prefix+prev_tag
        for prefix_len in range(min_prefix, max_prefix + 1):
            features['prev_prefix_%d+prev_tag' % prefix_len] = (prev_word[:prefix_len], prev_tag)

        # prev_suffix+prev_tag
        for suffix_len in range(min_suffix, max_suffix + 1):
            features['prev_suffix_%d+prev_tag' % suffix_len] = (prev_word[-suffix_len:], prev_tag)

    ### END YOUR CODE

    return features


def extract_features(sentence, i):
    curr_word = sentence[i][0]
    prev_token = sentence[i - 1] if i > 0 else ('<s>', '*')
    prevprev_token = sentence[i - 2] if i > 1 else ('<s>', '*')
    next_token = sentence[i + 1] if i < (len(sentence) - 1) else ('</s>', 'STOP')
    return extract_features_base(curr_word, next_token[0], prev_token[0], prevprev_token[0], prev_token[1],
                                 prevprev_token[1])


def vectorize_features(vec, features):
    """
        Receives: feature dictionary
        Returns: feature vector

        Note: use this function only if you chose to use the sklearn solver!
        This function prepares the feature vector for the sklearn solver,
        use it for tags prediction.
    """
    example = [features]
    return vec.transform(example)


def create_examples(sents, tag_to_idx_dict):
    examples = []
    labels = []
    num_of_sents = 0
    for sent in sents:
        num_of_sents += 1
        for i in xrange(len(sent)):
            features = extract_features(sent, i)
            examples.append(features)
            labels.append(tag_to_idx_dict[sent[i][1]])

    return examples, labels


def memm_greedy(sent, logreg, vec, index_to_tag_dict):
    """
        Receives: a sentence to tag and the parameters learned by memm
        Returns: predicted tags for the sentence
    """
    predicted_tags = [""] * (len(sent))
    ### YOUR CODE HERE
    tagged_sent = []
    for i, word in enumerate(sent):
        tagged_sent.append((word,))
        features = extract_features(tagged_sent, i)
        vec_features = vectorize_features(vec, features)
        pred_tag_idx = logreg.predict(vec_features)[0]
        pred_tag = index_to_tag_dict[pred_tag_idx]
        tagged_sent[-1] = (word, pred_tag)
        predicted_tags[i] = pred_tag
    ### END YOUR CODE
    return predicted_tags


def memm_viterbi(sent, logreg, vec, index_to_tag_dict,
                 candidates_per_word=3, return_context=False, print_logreg_times=False):
    """
        Receives: a sentence to tag and the parameters learned by hmm
        Returns: predicted tags for the sentence

        note - in our implementation, PI contains log-probabilities for numeric stability
        candidates_per_word: pruning. only the top tag candidates are kept.
        return_context: returns a list with context for every prediction
        print_times: print times spent performing logreg classification
    """
    predicted_tags = [""] * (len(sent))

    ### YOUR CODE HERE
    PI, BP = {}, {}
    if return_context:
        context = {}

    total_tags = index_to_tag_dict.values()
    total_tags.remove('*')

    t_logreg = 0
    num_logreg = 0

    PI[(-1, '*', '*')] = 0.
    for k in range(len(sent)):
        PI_k = {}
        BP_k = {}
        if return_context:
            context_k = {}

        relevant_curr_tags = word_to_relevant_tags_dict.get(sent[k], total_tags)
        for curr_tag in relevant_curr_tags:
            curr_tag_idx = tag_to_idx_dict[curr_tag]
            relevant_prev_tags = word_to_relevant_tags_dict.get(sent[k - 1], total_tags) if k > 0 else ['*']
            for prev_tag in relevant_prev_tags:
                curr_arr = []
                relevant_prevprev_tags = word_to_relevant_tags_dict.get(sent[k - 2], total_tags) if k > 1 else ['*']
                for prevprev_tag in relevant_prevprev_tags:
                    # only check tags that weren't pruned in stage k-1
                    if (k - 1, prevprev_tag, prev_tag) in PI:
                        prev_path_prob = PI[(k - 1, prevprev_tag, prev_tag)]

                        # build fake mini-sentence and extract features
                        i_word = 0
                        tagged_sent = [(sent[k], '')]
                        if k > 0:
                            tagged_sent.insert(0, (sent[k - 1], prev_tag))
                            i_word += 1
                        if k > 1:
                            tagged_sent.insert(0, (sent[k - 2], prevprev_tag))
                            i_word += 1
                        if k < len(sent) - 1:
                            tagged_sent.append((sent[k + 1], ''))

                        features = extract_features(tagged_sent, i_word)

                        # apply logreg classifier
                        t0 = time.time()
                        vec_features = vectorize_features(vec, features)
                        probs = logreg.predict_proba(vec_features)
                        curr_prob = probs.flat[curr_tag_idx]
                        t1 = time.time()
                        t_logreg += t1 - t0
                        num_logreg += 1

                        # add logprob
                        path_prob = prev_path_prob + np.log2(curr_prob)
                    else:
                        # if the path was pruned in stage k-1, mark it as irrelevant
                        path_prob = -np.inf

                    curr_arr.append(path_prob)

                # find the best path for prev_tag, curr_tag and add it to the tables
                i_max = np.argmax(curr_arr)
                max_curr_path_prob = curr_arr[i_max]
                best_prevprev_tag = relevant_prevprev_tags[i_max]
                if max_curr_path_prob > -np.inf:
                    PI_k[(k, prev_tag, curr_tag)] = max_curr_path_prob
                    BP_k[(k, prev_tag, curr_tag)] = best_prevprev_tag
                    if return_context:
                        context_k[(k, prev_tag, curr_tag)] = {
                            'curr_word': sent[k],
                            'next_word': sent[k + 1] if k < len(sent) - 1 else '</s>',
                            'prev_word': sent[k - 1] if k > 0 else '<s>',
                            'prevprev_word': sent[k - 2] if k > 1 else '<s>',
                            'prev_tag': prev_tag,
                            'prevprev_tag': best_prevprev_tag
                        }

        # perform pruning - keep top candidates from this stage
        if candidates_per_word is None:
            pruned_PI_k = PI_k
            pruned_BP_k = BP_k
            if return_context:
                pruned_context_k = context_k
        else:
            sorted_PI_k = sorted(PI_k.items(), key=lambda kv_pair: kv_pair[1], reverse=True)
            pruned_PI_k = sorted_PI_k[:candidates_per_word]
            pruned_BP_k = [(key, BP_k[key]) for key, val in pruned_PI_k]
            if return_context:
                pruned_context_k = [(key, context_k[key]) for key, val in pruned_PI_k]

        PI.update(pruned_PI_k)
        BP.update(pruned_BP_k)
        if return_context:
            context.update(pruned_context_k)

    # find the best ending 2-tag sequence
    PI_end = [(key, prob) for (key, prob) in PI.iteritems() if key[0] == len(sent) - 1]
    keys, probs = zip(*PI_end)
    best_key = keys[np.argmax(probs)]
    _, before_last_tag, last_tag = best_key
    predicted_tags[len(sent) - 2] = before_last_tag
    predicted_tags[len(sent) - 1] = last_tag

    # use back pointers to tag entire sentence
    for k in range(len(sent) - 3, -1, -1):
        predicted_tags[k] = BP[(k + 2, predicted_tags[k + 1], predicted_tags[k + 2])]

    if print_logreg_times:
        print 't_logreg:', t_logreg, ', num_logreg:', num_logreg

    # build the cotext list for every tagging in the sentence
    if return_context:
        predicted_context = [None] * len(sent)
        predicted_context[0] = context[(0, '*', predicted_tags[0])]
        for k in range(1, len(sent)):
            predicted_context[k] = context[(k, predicted_tags[k - 1], predicted_tags[k])]

        return predicted_tags, predicted_context

    return predicted_tags


def should_add_eval_log(sentene_index):
    if sentene_index > 0 and sentene_index % 10 == 0:
        if sentene_index < 150 or sentene_index % 200 == 0:
            return True

    return False


def memm_eval(test_data, logreg, vec, index_to_tag_dict):
    """
    Receives: test data set and the parameters learned by memm
    Returns an evaluation of the accuracy of Viterbi & greedy memm
    """
    acc_viterbi, acc_greedy = 0.0, 0.0
    eval_start_timer = time.time()

    num_tags = 0
    num_correct_greedy = 0
    num_correct_viterbi = 0

    for i, sen in enumerate(test_data):

        ### YOUR CODE HERE
        ### Make sure to update Viterbi and greedy accuracy

        num_tags += len(sen)

        sent_words, sent_tags = zip(*sen)

        predicted_tags_greedy = memm_greedy(sent_words, logreg, vec, index_to_tag_dict)
        curr_correct_greedy = np.sum(np.asarray(predicted_tags_greedy) == np.asarray(sent_tags))
        num_correct_greedy += curr_correct_greedy
        acc_greedy = float(num_correct_greedy) / num_tags

        predicted_tags_viterbi = memm_viterbi(sent_words, logreg, vec, index_to_tag_dict)
        curr_correct_viterbi = np.sum(np.asarray(predicted_tags_viterbi) == np.asarray(sent_tags))
        num_correct_viterbi += curr_correct_viterbi
        acc_viterbi = float(num_correct_viterbi) / num_tags

        ### END YOUR CODE

        if should_add_eval_log(i):
            if acc_greedy == 0 and acc_viterbi == 0:
                raise NotImplementedError
            eval_end_timer = time.time()
            print str.format("Sentence index: {} greedy_acc: {}    Viterbi_acc:{} , elapsed: {} ", str(i),
                             str(acc_greedy), str(acc_viterbi), str(eval_end_timer - eval_start_timer))
            eval_start_timer = time.time()

    return str(acc_viterbi), str(acc_greedy)


def gather_fail_stats(test_data, logreg, vec, index_to_tag_dict, num_analyze=100):
    """
    run memm_viterbi for random sentences (or for all of them is num_analyze=None)
    and return a list of failure stats: for every wrong prediction, return a dict
    with the correct tag, predicted tag, and context.
    """
    fail_stats = []
    test_data = np.random.permutation(test_data).tolist()

    print 'gathering samples to analyze:'
    i_sent = 0
    num_sentences = len(test_data)
    while (len(test_data) > 0) and ((num_analyze is None) or (len(fail_stats) < num_analyze)):
        sent = test_data.pop()
        sent_words, sent_tags = zip(*sent)
        predicted_tags, predicted_context = memm_viterbi(sent_words, logreg, vec, index_to_tag_dict,
                                                         return_context=True)
        fail_inds = np.where(np.asarray(predicted_tags) != np.asarray(sent_tags))[0]
        for i_fail in fail_inds:
            fail_stats.append({
                'correct_tag': sent_tags[i_fail],
                'predicted_tag': predicted_tags[i_fail],
                'context': predicted_context[i_fail]
            })

        if num_analyze is not None:
            print 'sentence %d/%d, fails %d/%d' % (i_sent + 1, num_sentences, len(fail_stats), num_analyze)
        else:
            print 'sentence %d/%d, fails %d' % (i_sent + 1, num_sentences, len(fail_stats))
        i_sent += 1

    if num_analyze is not None:
        fail_stats = fail_stats[:num_analyze]

    return fail_stats


def false_analysis(fail_stats):
    """
    analyze fail_stats returned by gather_fail_stats
    """
    correct_tags = [x['correct_tag'] for x in fail_stats]
    predicted_tags = [x['predicted_tag'] for x in fail_stats]

    datetime_str = datetime.datetime.now().strftime('%Y-%m-%d--%H-%M-%S')
    out_path = osp.join(osp.dirname(__file__), 'false_analysis', 'confmat@' + datetime_str + '.png')
    out_path_norm = osp.join(osp.dirname(__file__), 'false_analysis', 'confmat_norm@' + datetime_str + '.png')
    plot_confusion_matrix(correct_tags, predicted_tags, out_path=out_path, out_path_norm=out_path_norm)


def build_tag_to_idx_dict(train_sentences):
    curr_tag_index = 0
    tag_to_idx_dict = {}
    for train_sent in train_sentences:
        for token in train_sent:
            tag = token[1]
            if tag not in tag_to_idx_dict:
                tag_to_idx_dict[tag] = curr_tag_index
                curr_tag_index += 1

    tag_to_idx_dict['*'] = curr_tag_index
    return tag_to_idx_dict


def build_word_to_relevant_tags_dict(train_sentences, max_tags=None):
    train_tokens = [token for sent in train_sentences for token in sent]
    d = {}
    for word, tag in train_tokens:
        if word not in d:
            d[word] = [tag]
        else:
            d[word].append(tag)

    d_unique = {}
    for word, tag_list in d.iteritems():
        unique_tags, unique_counts = np.unique(tag_list, return_counts=True)
        unique_tags = unique_tags[np.argsort(-unique_counts)]
        if max_tags is not None:
            unique_tags = unique_tags[:max_tags]
        d_unique[word] = unique_tags

    return d_unique


if __name__ == "__main__":

    uni_features = True
    bi_features = True
    prefix_suffix_features = False
    replace_word_func = replace_word_advanced
    replace_all_numerics = False

    # backwards compatibility
    # uni_features = True
    # bi_features = True
    # prefix_suffix_features = False
    # replace_word_func = replace_word_simple
    # replace_all_numerics = True

    # backwards compatibility
    # uni_features = False
    # bi_features = False
    # prefix_suffix_features = False
    # replace_word_func = replace_word_simple
    # replace_all_numerics = True

    is_false_analysis = False
    load_pretrained = False
    is_train = True
    warm_start = False
    max_iter = 128
    assert load_pretrained or is_train

    full_flow_start = time.time()
    print (get_details())
    train_sents = read_conll_pos_file("data/Penn_Treebank/train.gold.conll")
    dev_sents = read_conll_pos_file("data/Penn_Treebank/dev.gold.conll")

    vocab = compute_vocab_count(train_sents)
    if replace_all_numerics:
        for k in vocab.keys():
            if re.match(r'.*\d.*', k):
                vocab.pop(k)

    train_sents = preprocess_sent(vocab, train_sents, replace_word_func=replace_word_advanced)
    dev_sents = preprocess_sent(vocab, dev_sents, replace_word_func=replace_word_advanced)
    tag_to_idx_dict = build_tag_to_idx_dict(train_sents)
    index_to_tag_dict = invert_dict(tag_to_idx_dict)
    word_to_relevant_tags_dict = build_word_to_relevant_tags_dict(train_sents, max_tags=5)

    # The log-linear model training.
    # NOTE: this part of the code is just a suggestion! You can change it as you wish!

    vec = DictVectorizer()
    print "Create train examples"
    train_examples, train_labels = create_examples(train_sents, tag_to_idx_dict)
    num_train_examples = len(train_examples)
    print "#example: " + str(num_train_examples)
    print "Done"

    print "Create dev examples"
    dev_examples, dev_labels = create_examples(dev_sents, tag_to_idx_dict)
    num_dev_examples = len(dev_examples)
    print "#example: " + str(num_dev_examples)
    print "Done"

    all_examples = train_examples
    all_examples.extend(dev_examples)

    print "Vectorize examples"
    all_examples_vectorized = vec.fit_transform(all_examples)
    print 'Number of Features:', len(vec.feature_names_)
    train_examples_vectorized = all_examples_vectorized[:num_train_examples]
    dev_examples_vectorized = all_examples_vectorized[num_train_examples:]
    print "Done"

    if load_pretrained:
        if uni_features and bi_features and not prefix_suffix_features:
            model_path = osp.join(osp.dirname(__file__),
                                  r'trained_models\prev_word+prev_tag@prevprev_word@word@prev_tag@next_word@prevprev_word+prevprev_tag@prevprev_tag+prev_tag@prevprev_tag@prev_word@@@2018-05-14--23-27-07.pkl')
        elif not bi_features and not prefix_suffix_features:
            # only 'word' feature
            model_path = osp.join(osp.dirname(__file__), r'trained_models\word@@@2018-05-12--09-01-23.pkl')
        elif not prefix_suffix_features:
            # words and tags
            model_path = osp.join(osp.dirname(__file__),
                                  r'trained_models\prev_word+prev_tag@prevprev_word@word@prev_word+next_word@prevprev_word+prev_word@next_word@prevprev_word+prevprev_tag@prevprev_tag+prev_tag@prevprev_tag@prev_tag@prev_word@@@2018-05-12--13-31-41.pkl')
        else:
            # suffixes and prefixes
            raise NotImplementedError

        with open(model_path, 'rb') as f:
            logreg = pkl.load(f)
    else:
        logreg = linear_model.LogisticRegression(
            multi_class='multinomial', solver='lbfgs', C=100000, verbose=1)

    if is_train:
        logreg.warm_start = warm_start
        logreg.max_iter = max_iter

        print "Fitting..."
        start = time.time()
        logreg.fit(train_examples_vectorized, train_labels)
        end = time.time()
        print "End training, elapsed " + str(end - start) + " seconds"

        print "Saving model..."
        trained_models_dir = 'trained_models'
        if not osp.exists(trained_models_dir):
            os.makedirs(trained_models_dir)
        datetime_str = datetime.datetime.now().strftime('%Y-%m-%d--%H-%M-%S')
        feature_names = '@'.join(train_examples[0].keys())
        model_path = osp.join(trained_models_dir, feature_names + '@@@' + datetime_str + '.pkl')
        with open(model_path, 'wb') as f:
            pkl.dump(logreg, f)
        print "Done"

    # False Analysis
    if is_false_analysis:
        fail_stats = gather_fail_stats(dev_sents, logreg, vec, index_to_tag_dict, num_analyze=None)
        datetime_str = datetime.datetime.now().strftime('%Y-%m-%d--%H-%M-%S')
        fail_stats_path = osp.join(osp.dirname(__file__), 'false_analysis', 'fail_stats@' + datetime_str + '.json')
        with open(fail_stats_path, 'w') as f:
            json.dump(fail_stats, f, indent=2)
        false_analysis(fail_stats)

    # Evaluation code - do not make any changes
    start = time.time()
    print "Start evaluation on dev set"
    acc_viterbi, acc_greedy = memm_eval(dev_sents, logreg, vec, index_to_tag_dict)
    end = time.time()
    print "Dev: Accuracy greedy memm : " + acc_greedy
    print "Dev: Accuracy Viterbi memm : " + acc_viterbi

    print "Evaluation on dev set elapsed: " + str(end - start) + " seconds"
    if os.path.exists('Penn_Treebank/test.gold.conll'):
        test_sents = read_conll_pos_file("Penn_Treebank/test.gold.conll")
        test_sents = preprocess_sent(vocab, test_sents)
        start = time.time()
        print "Start evaluation on test set"
        acc_viterbi, acc_greedy = memm_eval(test_sents, logreg, vec, index_to_tag_dict)
        end = time.time()

        print "Test: Accuracy greedy memm: " + acc_greedy
        print "Test:  Accuracy Viterbi memm: " + acc_viterbi

        print "Evaluation on test set elapsed: " + str(end - start) + " seconds"
        full_flow_end = time.time()
        print "The execution of the full flow elapsed: " + str(full_flow_end - full_flow_start) + " seconds"
