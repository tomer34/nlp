import os
import os.path as osp
import re
import pandas as pd
import tempfile
import shutil
import codecs

rogue_runnable_dir = osp.join(osp.dirname(__file__), 'rouge2-1.2.1-runnable')
default_rogue_jar = osp.join(rogue_runnable_dir, 'rouge2-1.2.1.jar')
default_template = osp.join(rogue_runnable_dir, 'rouge.properties')
default_stopwords_file = osp.join(rogue_runnable_dir, 'resources/stopwords-rouge-default.txt')


def write_summary_files(ref_summaries, pred_summaries, eval_dir):
    reference_dir = osp.join(eval_dir, 'reference')
    pred_dir = osp.join(eval_dir, 'system')

    if not osp.exists(reference_dir):
        os.makedirs(reference_dir)
    if not osp.exists(pred_dir):
        os.makedirs(pred_dir)

    for i_sum, summary in enumerate(ref_summaries):
        path = osp.join(reference_dir, str(i_sum) + '_ref.txt')
        if not osp.exists(path):
            summary_lines = '\n'.join(re.split('[.!?]', summary))
            with codecs.open(path, 'w', encoding='utf8') as f:
                f.write(summary_lines)

    for i_sum, summary in enumerate(pred_summaries):
        path = osp.join(pred_dir, str(i_sum) + '_pred.txt')
        summary_lines = '\n'.join(re.split('[.!?]', summary))
        with codecs.open(path, 'w', encoding='utf8') as f:
            f.write(summary_lines)


def write_properties_file(
        eval_dir,
        results_path,
        metrics,
        prop_template_path,
        remove_stopwords,
        stopwords_file):
    eval_dir = osp.abspath(eval_dir).replace('\\', '/')
    results_path = results_path.replace('\\', '/')
    prop_path = osp.join(eval_dir, 'rogue.properties').replace('\\', '/')
    ngram = ','.join(map(str, metrics))

    with open(prop_template_path, 'r') as f:
        props = f.read()

    props = re.sub(r'project.dir=.*', r'project.dir=' + eval_dir, props)
    props = re.sub(r'outputFile=.*', r'outputFile=' + results_path, props)
    props = re.sub(r'stopwords.file=.*', r'stopwords.file=' + stopwords_file, props)
    props = re.sub(r'ngram=.*', r'ngram=' + ngram, props)
    props = re.sub(r'stopwords.use=.*', r'stopwords.use=' + str(remove_stopwords).lower(), props)

    with open(prop_path, 'w') as f:
        f.write(props)

    return prop_path


def rogue_eval(ref_summaries, pred_summaries,
               eval_dir=None,
               results_path=None,
               metrics=(1, 2, 'L'),
               prop_template_path=default_template,
               remove_stopwords=True,
               stopwords_file=default_stopwords_file,
               rogue_jar=default_rogue_jar):
    """
    metrics: what type rouge-metric to use?
    options:1,2,3,..N; S1,S2,S3...SN; SU1, SU2, SU3...SUN; L
    """
    # if eval_dir isn't specified, create a temporary dir
    is_tmp_eval = False
    if eval_dir is None:
        is_tmp_eval = True
        eval_dir = tempfile.mkdtemp(prefix='rogue_eval__')

    if results_path is None:
        results_path = osp.join(eval_dir, 'results.csv')

    if not osp.exists(eval_dir):
        os.makedirs(eval_dir)

    write_summary_files(ref_summaries, pred_summaries, eval_dir)

    prop_path = write_properties_file(
        eval_dir, results_path, metrics, prop_template_path, remove_stopwords, stopwords_file)

    rogue_command_line = 'java -Drouge.prop="{prop_path}" -jar {rogue_jar}'.format(
        prop_path=prop_path, rogue_jar=rogue_jar)
    os.system(rogue_command_line)

    results_df = pd.read_csv(results_path)

    # if the eval dir is temporary, delete it
    if is_tmp_eval:
        shutil.rmtree(eval_dir)

    return results_df


def test():
    data = [
        {
            'text': 'This morning, Dana ate granola with dried fruit',
            'summary': 'Dana ate granola'
        },
        {
            'text': 'Tomer likes eating at Ratzon Falafel, usually while ordering extra tahini',
            'summary': 'Tomer likes Ratzon'
        }
    ]

    pred_summaries = [
        'Dana granola',
        'Ratzon the the the the the the'
    ]

    ref_summaries = [d['summary'] for d in data]

    results_path = '/home/dana/nlp/tomer/repo/Project/Master-Thesis-master/playground/rogue_results.csv'
    metrics = [1, 3, 'L']
    results_df = rogue_eval(ref_summaries, pred_summaries,
                            metrics=metrics, remove_stopwords=True,
                            results_path=results_path)
    print results_df['Avg_F-Score']


if __name__ == '__main__':
    test()
