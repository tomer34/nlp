Advanced Methods in Natural Language Processing – Spring 2018 - Assignment 1

################### Submitted Files ###################

The answers to the theoretical questions, as well as our explanation for Question 2/h can be found in answer directory.
All of our implementations can be found in the proper files as per the assignment requirements, as well as another q1_utils.py file.

################### BY ################################

Dana Cohen - cohedana@gmail.com - 314865973
Tomer Ronen - tomer.ronen34@gmail.com - 308492909
Or Latovitz - orlatovitz@gmail.com - 206261331