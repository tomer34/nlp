#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
rationale_dependent.py

"""
from options import load_arguments
from IO import create_embedding_layer, read_annotations, read_annotations_cls, \
    create_batches, read_rationales, input_file_to_batches, read_annotations_newsroom
import tensorflow as tf
import numpy as np
import os
import os.path as osp

from models import Model
from models_cls import Model as ClassificationModel


def main():
    print 'Parser Arguments'
    for key, value in args.__dict__.iteritems():
        print u'{0}: {1}'.format(key, value)

    # choose read_annotations function
    if args.newsroom_data:
        read_annotations_func = read_annotations_newsroom
    elif args.classification:
        read_annotations_func = lambda path: read_annotations_cls(path, args.aspect)
    else:
        read_annotations_func = read_annotations

    # ensure embeddings exist
    assert args.embedding, "Pre-trained word embeddings required."

    embed_layer = create_embedding_layer(
        args.embedding
    )

    max_len = args.max_len

    if args.exp_dir is not None:
        exp_dir = args.exp_dir
    else:
        exp_dir = 'exp{}'.format(args.exp_num)
    if not os.path.exists(exp_dir):
        os.makedirs(exp_dir)

    saves_dir = osp.join(exp_dir, 'saves')
    if not os.path.exists(saves_dir):
        os.makedirs(saves_dir)

    dump = osp.join(exp_dir, 'rationales.json')

    if args.train:
        train_x, train_y = read_annotations_func(args.train)
        train_x = [embed_layer.map_to_ids(x)[:max_len] for x in train_x]

    if args.dev:
        dev_x, dev_y = read_annotations_func(args.dev)
        dev_x = [embed_layer.map_to_ids(x)[:max_len] for x in dev_x]

    if args.load_rationale:
        rationale_data = read_rationales(args.load_rationale)
        for x in rationale_data:
            x["xids"] = embed_layer.map_to_ids(x["x"])

    if args.train:
        with tf.Graph().as_default() as g:
            # used to be set to 2345
            tf.set_random_seed(2345)
            np.random.seed(2345)

            with tf.Session() as sess:
                # initialize Model
                if not args.classification:
                    model = Model(
                        args=args,
                        embedding_layer=embed_layer,
                        nclasses=len(train_y[0])
                    )
                else:
                    model = ClassificationModel(
                        args=args,
                        embedding_layer=embed_layer,
                        nclasses=args.nclasses
                    )

                model.ready()

                # added this for testing
                model.train((train_x, train_y),
                            (dev_x, dev_y) if args.dev else None,
                            None,
                            rationale_data if args.load_rationale else None,
                            sess)

    if args.evaluate_cls_model is not None:
        assert args.load_model is not None, 'must supply a load_model to evaluate it'

        with tf.Graph().as_default() as g:
            # used to be set to 2345
            tf.set_random_seed(2345)
            np.random.seed(2345)

            with tf.Session() as sess:
                # initialize Model
                model = ClassificationModel(
                    args=args,
                    embedding_layer=embed_layer,
                    nclasses=args.nclasses
                )
                model.ready()

                print '\nLoading model from path', args.load_model, '...'
                saver = tf.train.Saver()
                saver.restore(sess, args.load_model)
                print 'Finished loading\n'

                print '\nEvaluating model'
                dump_file_name, dump_file_ext = osp.splitext(dump)
                evaluate_dump_file_path = dump_file_name + '_evaluate_cls' + dump_file_ext
                dev_batches_x, dev_batches_y = create_batches(dev_x, dev_y, args.batch, model.generator.padding_id)
                accuracy, con_mat = model.evaluate_cls(dev_batches_x, dev_batches_y, sess, evaluate_dump_file_path)
                print 'accuracy:', accuracy
                print 'con_mat:'
                print con_mat

                model.dump_rationales(dump, dev_batches_x, dev_batches_y, sess)

            print 'Finished! wrote evaluation results to {0}'.format(evaluate_dump_file_path)

    if args.infer_rationales is not None:
        assert args.load_model is not None, 'must supply a load_model path when inferring rationales'
        batches = input_file_to_batches(args.infer_rationales, embed_layer, args.batch,
                                        embed_layer.vocab_map["<padding>"])

        with tf.Graph().as_default() as g:
            # used to be set to 2345
            tf.set_random_seed(2345)
            np.random.seed(2345)

            with tf.Session() as sess:
                # initialize Model
                model = Model(
                    args=args,
                    embedding_layer=embed_layer,
                    nclasses=args.nclasses
                )
                model.ready()

                print '\nLoading model from path', args.load_model, '...'
                saver = tf.train.Saver()
                saver.restore(sess, args.load_model)
                print 'Finished loading\n'

                print '\nInferring rationales...'
                model.dump_rationales_inference(batches, dump, sess)
                print 'Finished! Dumped rationales to file %s\n' % dump


def reset_graph():
    if 'sess' in globals() and sess:
        sess.close()
    tf.reset_default_graph()


if __name__ == '__main__':
    args = load_arguments()

    # reset zie graph
    reset_graph()

    # start procedures
    main()
