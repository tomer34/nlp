import gzip
import json
import pandas as pd

MOST_FREQUENT_HOSTS = [
    'www.foxnews.com', 'www.nydailynews.com', 'www.nytimes.com',
    'www.theguardian.com', 'www.washingtonpost.com']


def get_data(path, max_rows=None):
    if max_rows is None:
        data = pd.read_json(path, orient='records', lines=True, compression='gzip')
    else:
        reader = pd.read_json(path, orient='records', lines=True, compression='gzip', chunksize=max_rows)
        data = next(reader)

    def url2host(url):
        return url.replace('http://', '').replace('https://', '').split('/')[0]

    data['host'] = data['url'].apply(url2host)

    data = data.loc[(~data['summary'].isnull()) & (data['summary'] != '')]

    return data


def filter_only_frequent_hosts(data):
    return data.loc[data['host'].isin(MOST_FREQUENT_HOSTS)]


def main():
    dev_path = '/home/dana/dev.data'
    dev_out_path = '/home/dana/nlp/shared/newsroom/dev.jsonl.gzip'
    # max_read = None
    max_rows = 10000

    full_dev_data = get_data(dev_path, max_rows)
    dev_data = filter_only_frequent_hosts(full_dev_data)

    dev_data.to_json(dev_out_path, orient='records', lines=True, compression='gzip')


if __name__ == "__main__":
    main()
