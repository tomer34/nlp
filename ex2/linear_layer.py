import numpy as np


def linear_layer(x, W, b):
    """
    :param x:  1  x  Dx
    :param W:  Dx x  Da
    :param b:  1  x  Da
    :return:
        a:  1  x Da
    """
    a = x.dot(W) + b
    return a


def linear_layer_grad(x, W, b, da):
    """
    :param x:   1  x  Dx
    :param W:   Dx x  Da
    :param b:   1  x  Da
    :param da:  1  x  Da
    :return:
        dx:  1  x  Dx
        dW:  Dx x  Da
        db:  1  x  Da
    """
    dx = da.dot(W.T)
    dW = x.T.dot(da)
    db = da.sum(axis=0, keepdims=True)
    return dx, dW, db


def sanity_check():
    N = 20
    Dx = 10
    Da = 8
    x = np.random.randn(N, Dx)
    W = np.random.randn(Dx, Da)
    b = np.random.randn(1, Da)
    a = linear_layer(x, W, b)
    assert a.shape == (N, Da)
    da = np.random.randn(N, Da)
    dx, dW, db = linear_layer_grad(x, W, b, da)
    assert dx.shape == x.shape
    assert dW.shape == W.shape
    assert db.shape == b.shape
    print 'success'


if __name__ == '__main__':
    sanity_check()

