import numpy as np
from util import print_sentence, read_conll
import itertools
import os.path as osp
import pandas as pd


def read_results_conll(fstream):
    """
    Reads a input stream @fstream (e.g. output of `open(fname, 'r')`) in CoNLL file format.
    @returns a list of examples [(tokens), (labels)]. @tokens and @labels are lists of string.
    """
    ret = []

    current_toks, current_lbls, current_preds = [], [], []
    for line in fstream:
        line = line.strip()
        if len(line) == 0 or line.startswith("-DOCSTART-"):
            if len(current_toks) > 0:
                assert len(current_toks) == len(current_lbls)
                ret.append((current_toks, current_lbls, current_preds))
            current_toks, current_lbls, current_preds = [], [], []
        else:
            assert "\t" in line, r"Invalid CONLL format; expected a '\t' in {}".format(line)
            tok, lbl, pred = line.split("\t")
            current_toks.append(tok)
            current_lbls.append(lbl)
            current_preds.append(pred)
    if len(current_toks) > 0:
        assert len(current_toks) == len(current_lbls)
        ret.append((current_toks, current_lbls, current_preds))
    return ret


preds_path = osp.join(osp.dirname(__file__), r"results\rnn\20180522_145644\rnn_predictions.conll")
with open(preds_path, 'r') as f:
    preds_conll = read_results_conll(f)

train_path = r"C:\Users\Tomer\university\NLP\repo\ex4\code\data\train.conll"
with open(train_path, 'r') as f:
    train_conll = read_conll(f)
train_sents, train_labels = zip(*train_conll)
train_words = np.unique(list(itertools.chain(*train_sents)))

failed_words = []
failed_unknown_words = []

unknown_word_fails = []
fails = []
for tokens, labels, preds in preds_conll:
    tokens, labels, preds = map(np.array, [tokens, labels, preds])
    curr_failed_words = tokens[labels != preds]
    failed_words.extend(curr_failed_words.tolist())
    if len(curr_failed_words) != 0:
        fails.append((tokens, labels, preds))
        curr_failed_unknown_words = curr_failed_words[~np.in1d(curr_failed_words, train_words)]
        failed_unknown_words.extend(curr_failed_unknown_words.tolist())
        if len(curr_failed_unknown_words) != 0:
            unknown_word_fails.append((tokens, labels, preds))

print('num failed tokens:', len(failed_words))
print('num failed unknown tokens:', len(failed_unknown_words))

failed_words_path = osp.join(osp.dirname(__file__), r"results\rnn\20180522_145644\failed_words.csv")
s = pd.Series(failed_words)
s.groupby(s).count().sort_values(ascending=False).to_csv(failed_words_path)

failed_unknown_words_path = osp.join(osp.dirname(__file__), r"results\rnn\20180522_145644\failed_unknown_words.csv")
s = pd.Series(failed_unknown_words)
s.groupby(s).count().sort_values(ascending=False).to_csv(failed_unknown_words_path)

fails_path = osp.join(osp.dirname(__file__), r"results\rnn\20180522_145644\eval_output_fails.txt")
with open(fails_path, 'w') as f:
    for tokens, labels, preds in fails:
        print_sentence(f, tokens, labels, preds)

unknown_words_fails_path = osp.join(osp.dirname(__file__), r"results\rnn\20180522_145644\eval_output_fails_unknown_words.txt")
with open(unknown_words_fails_path, 'w') as f:
    for tokens, labels, preds in unknown_word_fails:
        print_sentence(f, tokens, labels, preds)


pass
