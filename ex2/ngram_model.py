#!/usr/local/bin/python

from data_utils import utils as du
import numpy as np
import pandas as pd
import csv
import time

# Load the vocabulary
vocab = pd.read_table("data/lm/vocab.ptb.txt", header=None, sep="\s+",
                     index_col=0, names=['count', 'freq'], )

# Choose how many top words to keep
vocabsize = 2000
num_to_word = dict(enumerate(vocab.index[:vocabsize]))
word_to_num = du.invert_dict(num_to_word)

# Load the training set
docs_train = du.load_dataset('data/lm/ptb-train.txt')
S_train = du.docs_to_indices(docs_train, word_to_num)
docs_dev = du.load_dataset('data/lm/ptb-dev.txt')
S_dev = du.docs_to_indices(docs_dev, word_to_num)

def train_ngrams(dataset):
    """
        Gets an array of arrays of indexes, each one corresponds to a word.
        Returns trigram, bigram, unigram and total counts.
    """
    trigram_counts = dict()
    bigram_counts = dict()
    unigram_counts = dict()
    token_count = 0
    
    train_start_time = time.time()
    for sentence_array in dataset:
        token_count += sentence_array.size - 3        
        for i in range(1, sentence_array.size):
            unigram_key = sentence_array[i]
            if unigram_key not in unigram_counts:
                unigram_counts[unigram_key] = 1.0
            else:
                unigram_counts[unigram_key] += 1

            if i < 1: continue

            bigram_key = (sentence_array[i], sentence_array[i-1])
            if bigram_key not in bigram_counts:
                bigram_counts[bigram_key] = 1.0
            else:
                bigram_counts[bigram_key] += 1

            if i < 2: continue

            trigram_key = (sentence_array[i], sentence_array[i-1], sentence_array[i-2])
            if trigram_key not in trigram_counts:
                trigram_counts[trigram_key] = 1.0
            else:
                trigram_counts[trigram_key] += 1

    print "Training took:{0:.2f} seconds".format(time.time() - train_start_time)
    return trigram_counts, bigram_counts, unigram_counts, token_count

def get_sentence_log_prob(sentence_array, trigram_counts, bigram_counts, unigram_counts, train_token_count, lambda1, lambda2, lambda3):
    """
    Goes over a single sentence, and returns it's log prob according to the trained language model
    """
    sentence_log_prob = 0
    for i in range(2, sentence_array.size):
        unigram_key = sentence_array[i]
        bigram_key = (sentence_array[i], sentence_array[i-1])
        trigram_key = (sentence_array[i], sentence_array[i-1], sentence_array[i-2])

        unigram_prob_part = lambda3 * (unigram_counts[unigram_key] / train_token_count)
        bigram_prob_part = lambda2 * (bigram_counts[bigram_key] / unigram_counts[bigram_key[1]]) if bigram_key in bigram_counts else 0
        trigram_prob_part = lambda1 * (trigram_counts[trigram_key] / bigram_counts[(trigram_key[1], trigram_key[2])]) if trigram_key in trigram_counts else 0
        
        curr_word_prob = unigram_prob_part + bigram_prob_part + trigram_prob_part
        sentence_log_prob += np.log2(curr_word_prob)

    return sentence_log_prob


def evaluate_ngrams(eval_dataset, trigram_counts, bigram_counts, unigram_counts, train_token_count, lambda1, lambda2):
    """
    Goes over an evaluation dataset and computes the perplexity for it with
    the current counts and a linear interpolation
    """
    perplexity = 0

    sum_log_prob = 0.0
    dev_token_count = 0.0
    lambda3 = 1 - lambda1 - lambda2
    for sentence_array in eval_dataset:
        dev_token_count += sentence_array.size - 3
        curr_sentence_log_prob = get_sentence_log_prob(sentence_array, trigram_counts, bigram_counts, unigram_counts, train_token_count, lambda1, lambda2, lambda3)
        sum_log_prob += curr_sentence_log_prob

    l = sum_log_prob / float(dev_token_count)
    perplexity = np.power(2, -l)
    
    return perplexity

def grid_search_for_best_perplexity(S_dev, trigram_counts, bigram_counts, unigram_counts, token_count):
    """
    Using grid search to find best linear interpolation for lowest perplexity
    """
    print "Now grid-searching for the best perplexity"
    perplexity_dict = dict()
    for lambda1 in np.linspace(0, 1, 20):
        for lambda2 in np.linspace(0, 1 - lambda1, max(1, ((1 - lambda1) / 0.05))):
            curr_perplexity = evaluate_ngrams(S_dev, trigram_counts, bigram_counts, unigram_counts, token_count, lambda1, lambda2)
            print "lambda1: {0:.2f} lambda2: {1:.2f} received perplexity: {2:.2f}".format(lambda1, lambda2, curr_perplexity)
            perplexity_dict[(lambda1, lambda2)] = curr_perplexity

    print "Perplexity for trigram (lambda1 = 1, lambda2 = 0, lambda3 = 0): {0:.2f}".format(perplexity_dict[(1, 0)])
    print "Perplexity for bigram (lambda1 = 0, lambda2 = 1, lambda3 = 0): {0:.2f}".format(perplexity_dict[(0, 1)])
    print "Perplexity for unigram (lambda1 = 0, lambda2 = 0, lambda3 = 1): {0:.2f}".format(perplexity_dict[(0, 0)])

    best_perplexity_lambdas = min(perplexity_dict, key=perplexity_dict.get)
    best_perplexity = perplexity_dict[best_perplexity_lambdas]
    return best_perplexity, best_perplexity_lambdas

def test_ngram():
    """
    Use this space to test your n-gram implementation.
    """
    #Some examples of functions usage
    trigram_counts, bigram_counts, unigram_counts, token_count = train_ngrams(S_train)
    print "#trigrams: " + str(len(trigram_counts))
    print "#bigrams: " + str(len(bigram_counts))
    print "#unigrams: " + str(len(unigram_counts))
    print "#tokens: " + str(token_count)
    perplexity = evaluate_ngrams(S_dev, trigram_counts, bigram_counts, unigram_counts, token_count, 0.5, 0.4)
    print "#perplexity: " + str(perplexity)
    
    best_perplexity, best_perplexity_lambdas = grid_search_for_best_perplexity(S_dev, trigram_counts, bigram_counts, unigram_counts, token_count)
    print "Best Perplexity was: {0:.2f} and was received for lambda1 = {1:.2f} lambda2 = {2:.2f} lambda3 = {3:.2f}".format(best_perplexity, best_perplexity_lambdas[0], best_perplexity_lambdas[1], 1 - best_perplexity_lambdas[0] - best_perplexity_lambdas[1])

if __name__ == "__main__":
    test_ngram()