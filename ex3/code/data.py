import os
import re

MIN_FREQ = 3


def invert_dict(d):
    res = {}
    for k, v in d.iteritems():
        res[v] = k
    return res


def read_conll_pos_file(path):
    """
        Takes a path to a file and returns a list of word/tag pairs
    """
    sents = []
    with open(path, "r") as f:
        curr = []
        for line in f:
            line = line.strip()
            if line == "":
                sents.append(curr)
                curr = []
            else:
                tokens = line.strip().split("\t")
                curr.append((tokens[1], tokens[3]))
    return sents


def increment_count(count_dict, key):
    """
        Puts the key in the dictionary if does not exist or adds one if it does.
        Args:
            count_dict: a dictionary mapping a string to an integer
            key: a string
    """
    if key in count_dict:
        count_dict[key] += 1
    else:
        count_dict[key] = 1


def compute_vocab_count(sents):
    """
        Takes a corpus and computes all words and the number of times they appear
    """
    vocab = {}
    for sent in sents:
        for token in sent:
            increment_count(vocab, token[0])
    return vocab


def replace_word_simple(word):
    """
        Replaces rare words with categories (numbers, dates, etc...)
    """
    ### YOUR CODE HERE
    if re.match(r'.*\d.*', word):
        return 'NUMERIC'
    ### END YOUR CODE
    return "UNK"


def replace_word_advanced(word):
    """
        Replaces rare words with categories (numbers, dates, etc...)
    """
    # TIME,YEAR,DATE
    Capitalizedword = re.compile("^[A-Z][a-z]{2}")
    with_makaf = re.compile("\w-\w")
    # Uncapitalizedword = "^[a-z]{3}"
    finishes_s = re.compile('[A-z][a-z]{2}s$')
    finishes_ed = re.compile('[A-z][a-z]{2}ed$')
    finishes_ion = re.compile('[A-z][a-z]{2}ion$')
    finishes_ing = re.compile("ing$")
    finishes_ly = re.compile("[A-z][a-z]{2}ly$")
    decimal_num = re.compile("\d+\.\d+")
    num = re.compile("\d")
    three_capital_letters = re.compile("^[A-Z]{3}$")
    finishes_ial = re.compile("[a-z]{2}ial$")
    if three_capital_letters.search(word):
        return "THREE_CAP_LETTERS"
    elif with_makaf.search(word):
        return "WITH_MAKAF"
    elif finishes_ed.search(word):
        return "FINISHES_ED"
    elif finishes_ing.search(word):
        return "FINISHES_ING"
    elif finishes_ly.search(word):
        return "FINISHES_LY"
    elif Capitalizedword.search(word):
        return "CAP_WORD"
    elif num.search(word) or decimal_num.search(word):
        return "NUM_"
    elif finishes_s.search(word):
        return "FINISHES_S"
    elif finishes_ial.search(word):
        return "FINISHES_IAL"
    else:
        return "UNK"


def preprocess_sent(vocab, sents, replace_word_func=replace_word_advanced):
    """
        return a sentence, where every word that is not frequent enough is replaced
    """
    res = []
    total, replaced = 0, 0
    for sent in sents:
        new_sent = []
        for token in sent:
            if token[0] in vocab and vocab[token[0]] >= MIN_FREQ:
                new_sent.append(token)
            else:
                new_sent.append((replace_word_func(token[0]), token[1]))
                replaced += 1
            total += 1
        res.append(new_sent)
    print "replaced: " + str(float(replaced) / total)
    return res
