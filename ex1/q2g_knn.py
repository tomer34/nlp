import numpy as np
from q2e_word2vec import normalizeRows


def knn(vector, matrix, k=10):
    """
    Finds the k-nearest rows in the matrix with comparison to the vector.
    Use the cosine similarity as a distance metric.

    Arguments:
    vector -- A D dimensional vector
    matrix -- V x D dimensional numpy matrix.

    Return:
    nearest_idx -- A numpy vector consists of the rows indices of the k-nearest neighbors in the matrix
    """

    nearest_idx = []

    ### YOUR CODE
    matrix = normalizeRows(matrix)
    vector = normalizeRows(vector[np.newaxis, :]).ravel()
    sim = np.dot(matrix, vector)
    I = np.argsort(-sim)
    nearest_idx = I[:k]
    ### END YOUR CODE

    return nearest_idx


def test_knn():
    """
    Use this space to test your knn implementation by running:
        python knn.py
    This function will not be called by the autograder, nor will
        your tests be graded.
    """
    print "Running your tests..."
    ### YOUR CODE HERE

    indices = knn(np.array([0.2, 0.5]), np.array([[0, 0.5], [0.1, 0.1], [0, 0.5], [2, 2], [4, 4], [3, 3]]), k=2)
    assert 0 in indices and 2 in indices and len(indices) == 2

    sklearn_exists = True
    try:
        from sklearn.neighbors import NearestNeighbors
    except:
        sklearn_exists = False
        print 'no sklearn - skipping our tests'

    if sklearn_exists:
        num_tests = 10
        for i_test in range(num_tests):
            print 'test %d / %d' % (i_test + 1, num_tests)
            d = 4
            V = 100
            k = 10
            vector = np.random.rand(d)
            matrix = np.random.rand(V, d)
            nbrs = NearestNeighbors(metric='cosine', algorithm='brute').fit(matrix)
            _, sklearn_inds = nbrs.kneighbors(vector.reshape(1, -1), n_neighbors=k)
            my_inds = knn(vector, matrix, k=k)
            assert np.all(sklearn_inds == my_inds), 'sklearn inds and my inds mismatch!'
    ### END YOUR CODE


if __name__ == "__main__":
    test_knn()
