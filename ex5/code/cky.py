from PCFG import PCFG


def load_sents_to_parse(filename):
    sents = []
    with open(filename) as f:
        for line in f:
            line = line.strip()
            if line:
                sents.append(line)
    return sents


def build_tree(key, BP):
    i, j, X = key
    bp = BP[key]
    if len(bp) == 1:
        return '(' + X + ' ' + bp[0] + ')'
    Y, Z, s = bp
    return '(' + X + ' ' + build_tree((i, s, Y), BP) + ' ' + build_tree((s + 1, j, Z), BP) + ')'


def cky(pcfg, sent, print_tables=False):
    ### YOUR CODE HERE
    sent = map(str.lower, sent.split(' '))

    n = len(sent)
    real_nonterminals = pcfg.get_real_nonterminals()

    # table initialization
    PI = {}
    BP = {}
    for i in range(n):
        word = sent[i]
        for X in pcfg.get_preterminals(strict=False):
            weight = pcfg.get_weight(X, [word])
            if weight != 0:
                PI[(i, i, X)] = weight
                BP[(i, i, X)] = (word,)

    # algorithm
    for l in range(1, n):
        for i in range(n - l):
            j = i + l
            for X in real_nonterminals:
                rules = pcfg.get_expanding_rules(X)
                expansion_scores = {}
                for rhs, weight in rules:
                    weight /= pcfg.get_sum(X)
                    Y, Z = rhs
                    for s in range(i, j):
                        expansion_scores[(Y, Z, s)] = weight * PI.get((i, s, Y), 0) * PI.get((s + 1, j, Z), 0)

                best_expansion, best_score = max(expansion_scores.iteritems(), key=lambda x: x[1])
                if best_score != 0:
                    PI[(i, j, X)] = best_score
                    BP[(i, j, X)] = best_expansion

    if print_tables:
        import pprint
        pprint.pprint(PI)
        print
        pprint.pprint(BP)
        print

    root_key = (0, len(sent) - 1, 'ROOT')
    if root_key not in PI:
        return "FAILED TO PARSE!"

    return build_tree(root_key, BP)
    ### END YOUR CODE


if __name__ == '__main__':
    import sys

    pcfg = PCFG.from_file_assert_cnf(sys.argv[1])
    sents_to_parse = load_sents_to_parse(sys.argv[2])
    for sent in sents_to_parse:
        print cky(pcfg, sent)
