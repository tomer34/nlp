import re
import pandas as pd
from rogue import rogue_eval
import numpy as np
import json


def extract_first_sentences(text, max_sentences):
    end = len(text)
    for i_sent, match_obj in enumerate(re.finditer('[.!?]', text)):
        if i_sent > (max_sentences - 1):
            break
        end = match_obj.end()
    summary = text[:end]
    return summary


def main():
    max_sentences = 3
    data_path = '/home/dana/nlp/shared/newsroom/dev.jsonl.gzip'
    results_path = '/home/dana/nlp/shared/newsroom/lede3_rogue.csv'

    dev_data = pd.read_json(data_path, orient='records', lines=True, compression='gzip')

    dev_data = pd.DataFrame(dev_data)
    pred_summaries = dev_data['text'].apply(lambda text: extract_first_sentences(text, max_sentences))
    ref_summaries = dev_data['summary']

    results_df = rogue_eval(ref_summaries, pred_summaries, results_path=results_path)
    results_df.sort_values(by='Task Name', inplace=True)
    avg_f_score = np.mean(results_df.loc[results_df['ROUGE-Type'] == 'ROUGE-L+StopWordRemoval', 'Avg_F-Score'])
    interesting_df = results_df.loc[:, ['ROUGE-Type', 'Task Name', 'Avg_F-Score']]

    print avg_f_score


if __name__ == '__main__':
    main()
