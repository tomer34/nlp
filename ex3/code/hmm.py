from data import *
import time
from submitters_details import get_details
from tester import verify_hmm_model
import sys
import numpy as np

START_TAG = '*'
STOP_TAG = "STOP"

def hmm_train(sents):
    """
        sents: list of tagged sentences
        Returns: the q-counts and e-counts of the sentences' tags, total number of tokens in the sentences
    """

    print "Start training"
    total_tokens = 0
    q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts,e_tag_counts = {}, {}, {}, {}, {}
    ### YOUR CODE HERE
    
    for sentence in sents:
        total_tokens += len(sentence)
        for i in range(len(sentence)):
            word_tag_pair = sentence[i]

            if word_tag_pair not in e_word_tag_counts:
                e_word_tag_counts[word_tag_pair] = 1.0
            else:
                e_word_tag_counts[word_tag_pair] += 1

            q_uni_key = word_tag_pair[1]
            q_bi_key = (sentence[i][1], sentence[i-1][1] if i >= 1 else START_TAG)
            q_tri_key = (sentence[i][1], sentence[i-1][1] if i >= 1 else START_TAG, sentence[i-2][1] if i >= 2 else START_TAG)

            if q_uni_key not in q_uni_counts:
                q_uni_counts[q_uni_key] = 1.0
            else:
                q_uni_counts[q_uni_key] += 1

            if q_bi_key not in q_bi_counts:
                q_bi_counts[q_bi_key] = 1.0
            else:
                q_bi_counts[q_bi_key] += 1
            
            if q_tri_key not in q_tri_counts:
                q_tri_counts[q_tri_key] = 1.0
            else:
                q_tri_counts[q_tri_key] += 1

        sent_len = len(sentence)
        stop_q_bi_key = (STOP_TAG, sentence[sent_len - 1][1] if sent_len >= 1 else START_TAG)
        stop_q_tri_key = (STOP_TAG, sentence[sent_len - 1][1] if sent_len >= 1 else START_TAG, sentence[sent_len - 2][1] if sent_len >= 2 else START_TAG)
        
        if stop_q_bi_key not in q_bi_counts:
            q_bi_counts[stop_q_bi_key] = 1.0
        else:
            q_bi_counts[stop_q_bi_key] += 1
        
        if stop_q_tri_key not in q_tri_counts:
            q_tri_counts[stop_q_tri_key] = 1.0
        else:
            q_tri_counts[stop_q_tri_key] += 1
    
    e_tag_counts = q_uni_counts.copy()
    q_uni_counts[START_TAG] = len(sents)
    q_uni_counts[STOP_TAG] = len(sents)
    q_bi_counts[(START_TAG, START_TAG)] = len(sents)

    ### END YOUR CODE
    return total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts,e_tag_counts

def emission(x, y, total_tokens, e_word_tag_counts, e_tag_counts):
    emission_prob = e_word_tag_counts[(x, y)] / float(e_tag_counts[y]) if (y in e_tag_counts and (x, y) in e_word_tag_counts) else 0
    return emission_prob if emission_prob > 0 else -sys.maxint

def transition(v, u, w, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, lambda1, lambda2):
    lambda3 = 1 - lambda1 - lambda2
    unigram_key = v
    bigram_key = (v, u)
    trigram_key = (v, u, w)


    unigram_prob_part = lambda3 * (q_uni_counts[unigram_key] / total_tokens)
    bigram_prob_part = lambda2 * (q_bi_counts[bigram_key] / q_uni_counts[bigram_key[1]]) if bigram_key in q_bi_counts else 0
    trigram_prob_part = lambda1 * (q_tri_counts[trigram_key] / q_bi_counts[(trigram_key[1], trigram_key[2])]) if trigram_key in q_tri_counts else 0
    
    transition_prob = unigram_prob_part + bigram_prob_part + trigram_prob_part
    return transition_prob if transition_prob > 0 else -sys.maxint

def get_argmax_u_v(PI, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, lambda1, lambda2, total_tags, sent_len):
    results = {}
    for u in total_tags:
        for v in total_tags:
            if (sent_len - 1, u, v) not in PI: continue
            results[(u, v)] = PI[(sent_len - 1, u, v)] * transition(STOP_TAG, v, u, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, lambda1, lambda2)
    return max(results, key=results.get) if len(results) > 0 else (START_TAG, START_TAG)

def hmm_viterbi(sent, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts, e_tag_counts, lambda1, lambda2):
    """
        Receives: a sentence to tag and the parameters learned by hmm
        Returns: predicted tags for the sentence
    """
    predicted_tags = [""] * (len(sent))
    ### YOUR CODE HERE

    PI, BP = {}, {}
    total_tags = e_tag_counts.keys()

    PI[(-1, START_TAG, START_TAG)] = 1
    for k in range(len(sent)):
        for v in total_tags:
            curr_emission = emission(sent[k][0], v, total_tokens, e_word_tag_counts, e_tag_counts)
            if curr_emission == -sys.maxint : continue
            for u in (total_tags if k > 0 else [START_TAG]):    
                curr_arr = [ PI[(k - 1, w, u)] *
                 transition(v, u, w, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, lambda1, lambda2) *
                 curr_emission if (k - 1, w, u) in PI else 0
                    for w in (total_tags if k > 1 else [START_TAG])]
                PI[(k, u, v)] = max(curr_arr)
                BP[(k, u, v)] = curr_arr.index(PI[(k, u, v)])

    predicted_tags[len(sent) - 2], predicted_tags[len(sent) - 1] = get_argmax_u_v(PI, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, lambda1, lambda2, total_tags, len(sent))

    for k in range(len(sent) - 3, -1, -1):
        if (k + 2, predicted_tags[k + 1], predicted_tags[k + 2]) not in BP:
            predicted_tags[k] = START_TAG
        else:
            predicted_tags[k] = total_tags[BP[(k + 2, predicted_tags[k + 1], predicted_tags[k + 2])]]

    ### END YOUR CODE
    return predicted_tags


def hmm_eval(test_data, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts,e_tag_counts):
    """
    Receives: test data set and the parameters learned by hmm
    Returns an evaluation of the accuracy of hmm
    """
    print "Start evaluation"
    acc_viterbi = 0.0
    ### YOUR CODE HERE

    tokens_tested = 0
    for sentence in test_data:
        tokens_tested += len(sentence)
        predicted_tags = hmm_viterbi(sentence, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts,e_tag_counts, lambda1, lambda2)
        for word_tag_pair, predicted_tag in zip(sentence, predicted_tags):
            if word_tag_pair[1] == predicted_tag:
                acc_viterbi += 1
    acc_viterbi /= float(tokens_tested)

    ### END YOUR CODE

    return str(acc_viterbi)

## run this func to get best lambdas
def grid_search_for_best_accuracy(dev_sents, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts, e_tag_counts):
    """
    Using grid search to find best linear interpolation for highest accuracy
    """
    print "Now grid-searching for the best accuracy"
    hmm_accuracy_dict = dict()
    for lambda1 in np.linspace(0, 1, 20):
        for lambda2 in np.linspace(0, 1 - lambda1, max(1, ((1 - lambda1) / 0.05))):
            curr_accuracy = hmm_eval(dev_sents, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts,e_tag_counts,lambda1, lambda2)
            print "lambda1: {0:.3f} lambda2: {1:.3f} received accuracy: {2:.3f}".format(lambda1, lambda2, curr_accuracy)
            hmm_accuracy_dict[(lambda1, lambda2)] = curr_accuracy

    best_accuracy_lambdas = max(hmm_accuracy_dict, key=hmm_accuracy_dict.get)
    best_accuracy = hmm_accuracy_dict[best_accuracy_lambdas]
    print(best_accuracy, best_accuracy_lambdas)
    return best_accuracy, best_accuracy_lambdas

lambda1 = 0.68
lambda2 = 0.25

if __name__ == "__main__":
    print (get_details())
    start_time = time.time()
    train_sents = read_conll_pos_file("Penn_Treebank/train.gold.conll")
    dev_sents = read_conll_pos_file("Penn_Treebank/dev.gold.conll")
    vocab = compute_vocab_count(train_sents)

    train_sents = preprocess_sent(vocab, train_sents)
    dev_sents = preprocess_sent(vocab, dev_sents)
    total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts, e_tag_counts = hmm_train(train_sents)
    # best_accuracy, best_accuracy_lambdas = grid_search_for_best_accuracy(dev_sents, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts,
    #                               e_tag_counts)


    verify_hmm_model(total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts, e_tag_counts)
    acc_viterbi = hmm_eval(dev_sents, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts, e_word_tag_counts,e_tag_counts)
    print "Dev: Accuracy of Viterbi hmm: " + acc_viterbi

    train_dev_time = time.time()
    print "Train and dev evaluation elapsed: " + str(train_dev_time - start_time) + " seconds"

    if os.path.exists("Penn_Treebank/test.gold.conll"):
        test_sents = read_conll_pos_file("Penn_Treebank/test.gold.conll")
        test_sents = preprocess_sent(vocab, test_sents)
        acc_viterbi = hmm_eval(test_sents, total_tokens, q_tri_counts, q_bi_counts, q_uni_counts,
                                           e_word_tag_counts, e_tag_counts)
        print "Test: Accuracy of Viterbi hmm: " + acc_viterbi
        full_flow_end = time.time()
        print "Full flow elapsed: " + str(full_flow_end - start_time) + " seconds"