Advanced Methods in Natural Language Processing – Spring 2018 - Assignment 2

################### Submitted Files ###################

The answers to the theoretical questions, as well as the results of our code, can be found in the /results directory.
All of our implementations can be found in the proper files as per the assignment requirements.

################### Results Summary ###################

Question 1 -
Perplexity for the given lambdas in Q1a: 63.52
Best Perplexity in Q1b was: 62.39 and was received for lambda1 = 0.37 lambda2 = 0.52 lambda3 = 0.11.

Question 2 -
dev perplexity : 112.967665327

################### BY ################################

Dana Cohen - cohedana@gmail.com - 314865973
Tomer Ronen - tomer.ronen34@gmail.com - 308492909
Or Latovitz - orlatovitz@gmail.com - 206261331