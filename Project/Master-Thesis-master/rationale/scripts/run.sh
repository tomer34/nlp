#!/usr/bin/env bash

EXP_NAME=cls1
GPU=1

EXP_DIR=exp_${EXP_NAME}
TEE_LOG=${EXP_DIR}/tee_log.txt

mkdir -p ${EXP_DIR}
exec > >(tee -a ${TEE_LOG}) 2>&1

PYTHONUNBUFFERED=1 \
CUDA_VISIBLE_DEVICES=${GPU} \
python rationale_dependent.py \
--classification     --nclasses=3    \
--dump ""   --load_rationale=""     \
--save_debug_tensors \
--exp_num="_${EXP_NAME}"
