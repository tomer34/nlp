#!/usr/bin/env bash

GPU=1
EXP_DIR=/home/dana/nlp/tomer/repo/Project/Master-Thesis-master/exp/beer_cls
MODEL=/home/dana/nlp/tomer/repo/Project/Master-Thesis-master/rationale/exp_cls1/saves/best_model_0.8297.ckpt

mv /home/dana/nlp/tomer/repo/Master-Thesis-master/exp/beer_cls /home/dana/nlp/tomer/repo/Project/Master-Thesis-master/exp/beer_cls

TEE_LOG=${EXP_DIR}/tee_log.txt
mkdir -p ${EXP_DIR}
exec > >(tee -a ${TEE_LOG}) 2>&1

PYTHONUNBUFFERED=1 \
CUDA_VISIBLE_DEVICES=${GPU} \
python rationale_dependent.py \
--evaluate_cls_model    --train "" \
--classification        --nclasses=3    \
--exp_dir ${EXP_DIR}  \
--load_rationale=""     \
--load_model="${MODEL}"
