from data import *
from submitters_details import get_details
import tester
from  collections import Counter

def most_frequent_train(train_data):
    """
    Gets training data that includes tagged sentences.
    Returns a dictionary that maps every word in the training set to its most frequent tag.
    """
    vocab = dict()
    map_vocab = dict()
    for sent in train_data:
        for word, tag in sent:
            if word in vocab:
                vocab[word][tag] += 1
            else:
                vocab[word] = Counter()
    for word in vocab:
        map_vocab[word] = vocab[word].most_common()[0][0]
    return map_vocab




def most_frequent_eval(test_set, pred_tags):
    """
    Gets test data and tag prediction map.
    Returns an evaluation of the accuracy of the most frequent tagger.
    """
    all = 0
    correct = 0
    for sent in test_set:
        for word, tag in sent:
            if tag == pred_tags[word]:
                correct += 1
            all += 1
    accuracy = float(correct)/all
    return str(accuracy)

if __name__ == "__main__":
    print (get_details())
    train_sents = read_conll_pos_file("Penn_Treebank/train.gold.conll")
    dev_sents = read_conll_pos_file("Penn_Treebank/dev.gold.conll")
    vocab = compute_vocab_count(train_sents)
    train_sents = preprocess_sent(vocab, train_sents)
    dev_sents = preprocess_sent(vocab, dev_sents)

    model = most_frequent_train(train_sents)
    print "dev: most frequent acc: " + most_frequent_eval(dev_sents, model)

    tester.verify_most_frequent_model(model)

    if os.path.exists('Penn_Treebank/test.gold.conll'):
        test_sents = read_conll_pos_file("Penn_Treebank/test.gold.conll")
        test_sents = preprocess_sent(vocab, test_sents)
        print "test: most frequent acc: " + most_frequent_eval(test_sents, model)