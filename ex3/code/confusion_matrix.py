"""
Taken from the sklearn tutorial on confusion matrices
"""

import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import datetime

def plot_confusion_matrix_helper(cm, classes,
                                 normalize=False,
                                 normalization_type='row',
                                 title='Confusion matrix',
                                 cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        if normalization_type == 'row':
            row_sum = cm.sum(axis=1)[:, np.newaxis]
            row_sum[row_sum == 0] = 1
            cm = cm.astype('float') / row_sum
        elif normalization_type == 'global':
            cm = cm.astype('float') / cm.sum()
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        fmt_ij = fmt
        if cm[i, j] == 0:
            fmt_ij = '.0f'
        plt.text(j, i, format(cm[i, j], fmt_ij),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def plot_confusion_matrix(correct_tags, predicted_tags, out_path=None, out_path_norm=None):
    # Compute confusion matrix
    tag_names = np.unique(correct_tags + predicted_tags)
    cnf_matrix = confusion_matrix(correct_tags, predicted_tags, labels=tag_names)
    np.set_printoptions(precision=2)

    # Plot non-normalized confusion matrix
    plt.figure(figsize=(12, 9))
    plot_confusion_matrix_helper(cnf_matrix, classes=tag_names,
                                 title='Confusion matrix, without normalization')
    if out_path is not None:
        plt.savefig(out_path)

    # Plot normalized confusion matrix
    plt.figure(figsize=(12, 9))
    plot_confusion_matrix_helper(cnf_matrix, classes=tag_names, normalize=True, normalization_type='global',
                                 title='Normalized confusion matrix')
    if out_path_norm is not None:
        plt.savefig(out_path_norm)

    plt.show()
