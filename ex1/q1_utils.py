#!/usr/bin/env python

import numpy as np

def run_function_on_every_row(x, func):
    """
    run func on every row of x

    Arguments:
    x -- A scalar or numpy array.

    Return:
    x -- x after running func on every row
    """

    orig_shape = x.shape

    if len(x.shape) > 1:
        # # Matrix
        x = np.apply_along_axis(func, axis=1, arr=x)
    else:
        # Vector
        x = func(x)

    assert x.shape == orig_shape
    return x